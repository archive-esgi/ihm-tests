run:
	docker-compose up -d
	cd backend; npm run dev

dcud:
	docker-compose up -d

mocha:
	cd backend; npm run test

enzyme:
	cd frontend; npm run test

cypress:
	cd frontend; npm run cypress

syntax-check:
	cd frontend; npm run syntax-check

front-install:
	cd frontend; npm i

back-install:
	cd backend; npm i