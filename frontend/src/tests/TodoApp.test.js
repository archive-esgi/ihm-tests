import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import TodoApp from '../components/TodoApp';

describe('Test TodoApp composant', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<TodoApp />);
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it('should render TodoApp component without error', () => {
    const div = document.createElement('div');
    ReactDOM.render(<TodoApp />, div);
  });

  it('should have a search field', () => {
    expect(wrapper.find('#todo-input').length).toEqual(1);
  });

  it('should have add todo button ', () => {
    expect(wrapper.find('#todo-add-button').length).toEqual(1);
  });

  it('Change search field value ', () => {
    wrapper.find('#todo-input').simulate('change', {
      target: { value: 'test de composant' },
    });
    expect(wrapper.find('input').prop('value')).toEqual('test de composant');
  });
});
