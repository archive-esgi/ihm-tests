import axios from 'axios';
import {
  SET_AUTHENTIFICATION,
  INCREMENT_ACTION_COUNT,
  ADD_RESSOURCES,
  PARSE_MESSAGE,
  PARSE_ERROR,
  RESET_ERROR,
} from './action-types';

const BASE_URL = 'http://localhost:3002';

// action creator
export const setAuthentification = isLoggedIn => ({
  type: SET_AUTHENTIFICATION,
  payload: isLoggedIn,
});

export const incrementActionCount = () => ({
  type: INCREMENT_ACTION_COUNT,
});

export const addRessources = () => ({
  type: ADD_RESSOURCES,
});

export const parseError = errorMessage => ({
  type: PARSE_ERROR,
  payload: errorMessage,
});

export const signinUser = ({ email, password }, history) => function (dispatch) {
  axios.post(`${BASE_URL}/signin`, {
    email,
    password,
  })
    .then((res) => {
      localStorage.setItem('token', res.data.token);
      dispatch(setAuthentification(true));
      history.push('/ressources');
    }).catch(() => {
      dispatch(parseError('Identifiants incorrects'));
    });
};

export const signoutUser = () => function (dispatch) {
  dispatch(setAuthentification(false));
  localStorage.removeItem('token');
};

export const signupUser = ({ email, password }, history) => function (dispatch) {
  axios.post(`${BASE_URL}/signup`, {
    email,
    password,
  })
    .then((res) => {
      localStorage.setItem('token', res.data.token);
      dispatch(setAuthentification(true));
      history.push('/ressources');
    }).catch(() => {
      dispatch(parseError('Email déjà utilisé'));
    });
};

export const getSecretRessource = () => function (dispatch) {
  axios
    .get(`${BASE_URL}/secretRessource`, {
      headers: {
        authorization: localStorage.getItem('token'),
      },
    })
    .then((res) => {
      dispatch({
        type: PARSE_MESSAGE,
        payload: res.data.result,
      });
    }).catch((err) => {
      console.log(err);
    });
};

export const resetError = () => ({
  type: RESET_ERROR,
});
