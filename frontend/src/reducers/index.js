import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import AuthentificationReducer from './Authentification';
import ActionInfoReducer from './ActionInfo';
import RessourcesReducer from './Ressources';
import ErrorsReducer from './Errors';

const rootReducer = combineReducers({
  form,
  authentification: AuthentificationReducer,
  ressources: RessourcesReducer,
  actionInfo: ActionInfoReducer,
  errors: ErrorsReducer,
});

export default rootReducer;
