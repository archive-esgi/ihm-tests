describe('home page (unlogged) test', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.get("a[href='/signout']").should('not.be.visible');
  });

  it('should show home page with title', () => {
    cy.get('.container h1').should(($el) => {
      expect($el[0].outerText).to.contain('Page d\'accueil');
    });
  });

  it('should show signin page with form on \'Connexion\' link click', () => {
    cy.get("a[href='/signin']").click();
    cy.url().should('include', '/signin');
    cy.get('h1').should(($el) => {
      expect($el[0].outerText).to.contain('Connexion');
    });
    cy.get('input[name=\'email\']');
    cy.get('input[name=\'password\']');
    cy.get('form button').should(($el) => {
      expect($el[0].outerText).to.contain('CONNEXION', {
        matchCase: true,
      });
    });
  });

  it('should show signup page with form on \'Inscription\' link click ', () => {
    cy.get("a[href='/signup']").click();
    cy.url().should('include', '/signup');
    cy.get('h1').should(($el) => {
      expect($el[0].outerText).to.contain('Inscription');
    });
    cy.get('input[name=\'email\']');
    cy.get('input[name=\'password\']');
    cy.get('input[name=\'secondPassword\']');
    cy.get('form button').should(($el) => {
      expect($el[0].outerText).to.contain('INSCRIPTION', {
        matchCase: true,
      });
    });
  });
});

describe('home page (logged) test', () => {
  beforeEach(() => {
    cy.exec('npm run db:reset');
    cy.request('POST', 'http://localhost:3002/signup', {
      email: 'cle@gmail.com',
      password: 'Azerty123',
    });
    cy.visit('/');
    cy.get("a[href='/signout']").should('not.be.visible');
    cy.visit('/signin');
    cy.get('input[name=\'email\']').type('cle@gmail.com');
    cy.get('input[name=\'password\']').type('Azerty123');
    cy.get('form button').click();
    cy.url('eq', '/ressources');
    cy.get("a[href='/signout']").should('be.visible');
  });

  it('should have access to /todo', () => {
    cy.visit('/todo');
  });
});
