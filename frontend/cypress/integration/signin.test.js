describe('signin page (unlogged) test', () => {
  beforeEach(() => {
    cy.visit('/signin');
    cy.get("a[href='/signout']").should('not.be.visible');
  });

  it('should display \'Identifiants incorrects\' error message', () => {
    cy.get('alert alert-danger').should('not.be.visible');
    cy.get('input[name=\'email\']').type('cypress@gmail.com');
    cy.get('input[name=\'password\']').type('wrongPassword');
    cy.get('form button').click();
    cy.get('#root > div > div.container.body_content > div').should('be.visible');
  });

  it('should redirect to /ressources on success login', () => {
    cy.exec('npm run db:reset');
    cy.request('POST', 'http://localhost:3002/signup', {
      email: 'signin@gmail.com',
      password: 'password',
    })
      .then((response) => {
        expect(response.body).to.have.property('token');
      });
    cy.get('alert alert-danger').should('not.be.visible');
    cy.get('input[name=\'email\']').type('signin@gmail.com');
    cy.get('input[name=\'password\']').type('password');
    cy.get('form button').click();
    cy.url().should('include', '/ressources');
    cy.get("a[href='/signout']").should('be.visible');
  });
});
