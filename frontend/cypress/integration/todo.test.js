describe('todo page CRUD (logged) test', () => {
  beforeEach(() => {
    cy.exec('npm run db:reset');
    cy.request('POST', 'http://localhost:3002/signup', {
      email: 'cle@gmail.com',
      password: 'Azerty123',
    });
    cy.visit('/signin');
    cy.get('input[name=\'email\']').type('cle@gmail.com');
    cy.get('input[name=\'password\']').type('Azerty123');
    cy.get('form button').click();
    cy.url('eq', '/ressources');
    cy.get("a[href='/signout']").should('be.visible');
    cy.visit('/todo');
  });

  it('should display todo input', () => {
    cy.get('#todo-input');
  });

  it('should display todo add button', () => {
    cy.get('#todo-add-button');
  });

  it('should display message when dont have todos', () => {
    cy.get('#no-todo-found').should(($el) => { expect($el[0].outerText).to.contain('Il y a actuellement 0 tâche'); });
  });

  it('should have todo displayed after add', () => {
    cy.get('input[id=\'todo-input\']').type('add todo test');
    cy.get('#todo-add-button').click();
    cy.get('.todo-item').should(($el) => { expect($el[0].outerText).to.contain('add todo test'); });
  });

  it('should have text \'Il y a actuellement 0 tâche\' after deleted todo ', () => {
    cy.get('input[id=\'todo-input\']').type('add todo test');
    cy.get('#todo-add-button').click();
    cy.get('.todo-item').should(($el) => { expect($el[0].outerText).to.contain('add todo test'); });
    cy.get('.delete').click();
    cy.get('#no-todo-found').should(($el) => { expect($el[0].outerText).to.contain('Il y a actuellement 0 tâche'); });
  });

  it('todo should have Completed status after updating', () => {
    cy.get('input[id=\'todo-input\']').type('add todo test');
    cy.get('#todo-add-button').click();
    cy.get('.todo-item').click();
    cy.get('.completed');
  });

  it('should display alert : La tâche existe déjà', () => {
    cy.get('input[id=\'todo-input\']').type('todo test');
    cy.get('#todo-add-button').click();
    cy.get('input[id=\'todo-input\']').type('todo test');
    cy.get('#todo-add-button').click();
    cy.on('window:alert', (string) => {
      expect(string).to.equal('La tâche existe déjà');
    });
  });
});
