describe('signout page (logged) test', () => {
  it('should have disconnected succes text', () => {
    cy.visit('/signin');
    cy.exec('npm run db:reset');
    cy.request('POST', 'http://localhost:3002/signup', {
      email: 'cle@gmail.com',
      password: 'Azerty123',
    });
    cy.get('input[name=\'email\']').type('cle@gmail.com');
    cy.get('input[name=\'password\']').type('Azerty123');
    cy.get('form button').click();
    cy.url('eq', '/ressources');
    cy.url().should('include', '/ressources');
    cy.get("a[href='/signout']").should('be.visible');
    cy.get('a[href=\'/signout\']').click();
    cy.url().should('include', '/signout');
    cy.get('.body_content > div').should(($el) => {
      expect($el[0].outerText).to.contain('Vous avez été déconnecté');
    });
    cy.get("a[href='/signout']").should('not.be.visible');
    cy.get('a[href=\'/signin\']');
    cy.get('a[href=\'/signup\']');
  });
});
