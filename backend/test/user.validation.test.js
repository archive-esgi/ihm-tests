const assert = require('assert');
const expect = require('chai').expect;
const User = require('../models/userModel');

describe('Validation tests on user', () => {
    let user1;

    beforeEach(done => {
        user1 = new User({
            email: 'test@gmail.com',
            password: 'testtest'
        });
        user1.save().then(() => {
            done();
        });
    });

    it('password should have more than 8 characters', done => {
        expect(user1.password).to.be.lengthOf.above(8);
        done();
    });
});
