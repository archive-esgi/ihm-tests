const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

before(done => {
    mongoose.connect('mongodb://127.0.0.1:27017/tests');
    mongoose.connection.once('open', () => {
        console.log('Connexion établie');
        done();
    })
    .on('error', error => {
        console.warn('Erreur durant la connexion', error)
    });
})

beforeEach('Supprime les données' ,(done) => { 
    mongoose.connection.db.dropDatabase().then( () => {
        done();
    });
});
