const assert = require('assert'); 
const User = require('../models/userModel');

// fonction contenant 1 collection de tests
// prends 2 param, nom de la collection + les tests
describe('Test CRUD on user', () => {
    let user1;

    beforeEach(done => {
        user1 = new User({
            email: 'test@gmail.com',
            password: 'testtest'
        });
        user1.save().then(() => {
            done();
        });
    });

    // un test, 2 params : nom du test + corps du test
    it('should create user', done => {
        const user2 = new User({
            email: 'test1@gmail.com',
            password: 'test1'
        });
        user2.save().then(() => {
            assert(!user2.isNew);
            done();
        });
    });

    it('should find user by id', done => {
        User.findOne({
            _id: user1.id
        })
        .then(user => {
            assert.equal(user.email, user1.email);
            done();
        });
    });

    it('should find user by id then delete it', done => {
        User.findOneAndDelete({
            _id: user1.id
        })
        .then(() => {
            User.findOne({
                _id: user1.id
            })
            .then(user => {
                assert.equal(user, null);
                done();
            });
        });
    });

    it('should find user by id then update it', done => {
        let newEmail = 'test10@gmail.com';
        User.findOneAndUpdate(
            user1.id,
            {email: newEmail}
        )
        .then(() => {
            User.find({})
            .then(user => {
                assert.equal(user[0].email, newEmail);
                done();
            });
        });
    });

    it('Rechercher un user par son email et le mettre à jour', done => {
        let newEmail = 'test11@gmail.com';
        User.findOneAndUpdate(
            user1.email,
            {email: newEmail}
        )
        .then(() => {
            User.find({})
            .then(user => {
                assert.equal(user[0].email, newEmail);
                done();
            });
        });
    });
});

