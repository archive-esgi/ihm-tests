const supertest = require("supertest");
const assert = require('assert');
const expressServer = require('../../server'); // routes
const request = require('supertest');
const Todo = require('../../models/todoModel');

describe('GET /todos', () => {
    it('it should has status code 200', done => {
        request(expressServer)
            .get('/todos')
            .expect(200)
            .expect('Content-Type',/json/) // header - value
            .end((err, res) => {
                if (err) done(err);
                done();
            });
        });
});

describe('POST /todos', () => {
    it('it should has status code 201 if task created and exists', done => {
        request(expressServer)
            .post('/todos')
            .send({task: 'ramenez la coupe à la maison avant'})
            .expect(201)
            .expect('Content-Type',/json/) // header - value
            .end((err, res) => {
                if (err) done(err);
                done();
            });
        });

    it('it shoud return status code 500 if we dosent send anything', done => {
        request(expressServer)
            .post('/todos')
            .send({})
            .expect(500)
            .end((err, res) => {
                if (err) done(err);
                done();
            });
        });
});

describe('DELETE /todos/:id', async () => {
    const todo = await new Todo({
        task: 'test DELETE'
    });
    todo.save();

    it('it should has status code 204 if task deleted', (done) => {
        request(expressServer)
            .del(`/todos/${todo._id}`)
            .expect(204)
            .end((err, res) => {
                if (err) done(err);
                done();
            });
        });

    it('it should has status code 204 if task deleted', (done) => {
        request(expressServer)
            .del(`/todos/312313`)
            .expect(500)
            .end((err, res) => {
                if (err) done(err);
                done();
            });
        });
});