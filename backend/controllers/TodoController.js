const Todo = require("../models/todoModel"); 

exports.findAll = (req, res, next) => {
    Todo.find()
        .then(data => res.json(data))
        .catch(err => res.sendStatus(500)
    );
}

exports.addTodo = (req, res) => {
    const todo = new Todo(req.body);
    todo.save()
        .then(data => res.status(201).json(data))
        .catch(err => res.sendStatus(500))
};

exports.deleteTodo = (req, res) => {
    Todo.findByIdAndDelete(req.params.id)
        .then((data) => (data ? res.sendStatus(204) : res.sendStatus(404)))
        .catch(() => res.sendStatus(500));
}

exports.updateTodo = (req, res) => {
    Todo.findByIdAndUpdate(req.params.id, req.body, {new: true})
        .then((data) => (data ? res.json(data) : res.sendStatus(404)))
        .catch((err) => {
        if (err.name === "ValidationError") {
            res.status(400).json(prettifyErrors(Object.values(err.errors)));
        } else {
            res.sendStatus(500);
        }
    });
}